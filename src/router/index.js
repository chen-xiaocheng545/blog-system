import Vue from "vue";
import VueRouter from "vue-router";

const originalPush = VueRouter.prototype.push;
//解决报错信息：NavigationDuplicated: Avoided redundant navigation to current location
VueRouter.prototype.push = function push(location) {
  return originalPush.call(this, location).catch((err) => err);
};

Vue.use(VueRouter);

let routes = [
  {
    path: "/",
    component: () => import("../views"),
  },
  {
    path: "/detail",
    component: () => import("../views/homePgae/Detail"),
  },
  {
    path: "/login",
    component: () => import("../views/Login"),
  },
  {
    path: "/dashboard",
    component: () => import("../views/dashboard"),
    redirect: "/dashboard/article",
    children: [
      {
        path: "/dashboard/article",
        component: () => import("../views/dashboard/article"),
        redirect: "/dashboard/article/center",
        children: [
          {
            path: "/dashboard/article/center",
            component: () => import("../views/dashboard/article/center"),
          },
          {
            path: "/dashboard/article/add",
            component: () => import("../views/dashboard/article/update"),
          },
          {
            name: "update",
            path: "/dashboard/article/update",
            component: () => import("../views/dashboard/article/update"),
          },
        ],
      },
      {
        path: "/dashboard/category",
        component: () => import("../views/dashboard/category"),
      },
    ],
  },
];

const router = new VueRouter({
  mode: "history", // hash  history
  routes,
});

export default router;
