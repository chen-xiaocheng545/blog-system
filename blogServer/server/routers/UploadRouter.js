const express = require("express");
const router = express.Router();
const fs = require("fs");
const { db, genid } = require("../db/DbUtils");
const { jwtCheak } = require("../utils/jwt");

router.post("/editor_upload", jwtCheak, async (req, res) => {
  // 没有文件  req.files: []
  if (!req.files || req.files.length === 0) {
    res.send({
      errno: 1, // 只要不等于 0 就行
      message: "上传失败",
    });
    return;
  }
  let files = req.files;
  console.log(files);
  let ret_files = [];
  for (let file of files) {
    // 获取文件名字后缀
    let file_ext = file.originalname.substring(
      file.originalname.lastIndexOf(".") + 1
    );
    // 随机文件名字
    let file_name = genid.NextId() + "." + file_ext;

    // 修改名字加移动文件
    // process.cwd() 程序运行时目录的路径
    fs.renameSync(
      process.cwd() + "/public/upload/temp/" + file.filename,
      process.cwd() + "/public/upload/" + file_name
    );
    ret_files.push("/upload/" + file_name);
  }

  res.send({
    errno: 0, // 注意：值是数字，不能是字符串
    data: {
      url: ret_files[0], // 图片 src ，必须
    },
  });
});

module.exports = router;
