const formatDate = function (time, format = "yyyy_mm_dd") {
  let date = new Date(time);
  let yyyy = date.getFullYear();
  let mm =
    date.getMonth() + 1 > 10
      ? date.getMonth() + 1
      : "0" + (date.getMonth() + 1);
  let dd = date.getDate() > 10 ? date.getDate() : "0" + date.getDate();
  let hours = date.getHours();
  let minutes = date.getMinutes();
  let seconds = date.getSeconds();
  switch (format) {
    case "yyyy-mm-dd":
      return yyyy + "-" + mm + "-" + dd;
    case "yyyy_mm_dd":
      return (
        yyyy +
        "年" +
        mm +
        "月" +
        dd +
        "日  " +
        hours +
        ":" +
        minutes +
        ":" +
        seconds
      );
    default:
      break;
  }
};
const throttle = function (fn, timeout = 10) {
  let timer;

  return function (...args) {
    if (timer) {
      timer = clearTimeout(timer);
    }
    timer = setTimeout(() => fn.apply(this, [...args]), timeout);
  };
};
export { formatDate, throttle };
