const express = require("express");
const router = express.Router();
const { db, genid } = require("../db/DbUtils");

router.get("/test", async (req, res) => {
  // 原始方法   回调函数
  //   db.all("select * from `admin`", [], (err, rows) => {
  //     console.log(rows);
  //   });

  //   promise   then回调
  //   db.async.all("select * from `admin`", []).then((res) => {
  //     console.log(res);
  //   });

  //   终极方法  async await
  let response = await db.async.all("select * from `admin`", []);
  console.log(response);
  res.send({
    id: genid.NextId(),
    data: response,
  });
});

module.exports = router;
