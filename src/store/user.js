import { get_local_storage, getToken } from "../utils/storage";

export default {
  namespaced: true,
  state: {
    token: getToken() || "",
    id: get_local_storage("id") || 0,
    account: get_local_storage("account") || "",
  },
  mutations: {
    SET_TOKEN(state, token) {
      state.token = token;
    },
    SET_ID(state, id) {
      state.id = id;
    },
    SET_ACCOUNT(state, account) {
      state.account = account;
    },
    REMOVE_TOKEN(state, token) {
      state.token = "";
    },
  },
  actions: {},
};
