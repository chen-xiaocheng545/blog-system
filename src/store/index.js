import Vue from "vue";
import Vuex from "vuex";
Vue.use(Vuex);
import user from "./user";
import category from "./category";
import blog from "./blog";
export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  getters: {
    isLoggedIn: (state) => state.user.token,
  },
  modules: {
    user,
    category,
    blog,
  },
});
