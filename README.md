# boke

前台直接 npm install 安装依赖
npm run serve 启动前端

后台同样 npm install 安装依赖
node app.js 启动后端服务

浏览器进入 localhost:8080 即可

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
