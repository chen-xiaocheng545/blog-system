const express = require("express");
const router = express.Router();
const { v4: uuidv4 } = require("uuid");
const { db, genid } = require("../db/DbUtils");

/**
 * 登录接口
 * params： account   password
 */
router.post("/login", async (req, res) => {
  let { account, password } = req.body;
  let { err, rows } = await db.async.all(
    "select * from `admin` where account = ? AND password = ?",
    [account, password]
  );
  if (!err && rows.length > 0) {
    let login_token = uuidv4();
    let update_token_sql = "UPDATE `admin` SET `token` = ? where `id` = ?";

    await db.async.run(update_token_sql, [login_token, rows[0].id]);

    let admin_info = JSON.parse(JSON.stringify(rows[0]));
    admin_info.token = login_token;
    delete admin_info.password;

    res.send({
      code: 200,
      msg: "登陆成功",
      data: admin_info,
    });
  } else {
    res.send({
      code: 500,
      msg: "账号或密码错误",
    });
  }
});

module.exports = router;
