import Vue from "vue";
const { $req } = Vue.prototype;
import { formatDate } from "../utils/util";

export default {
  namespaced: true,
  state: {
    blogList: [],
    blogPage: {},
    keyword: "",
  },
  mutations: {
    GET_BLOG_DATA(state, { blogData, isInterserct }) {
      if (!isInterserct) {
        // 分页展示
        state.blogList = blogData.list.map((item) => {
          if (item.content.length >= 199) {
            item.content += "...";
          }
          item.create_time = formatDate(item.create_time);
          return item;
        });
      } else {
        // 无限下拉
        blogData.list.forEach((item) => {
          item.create_time = formatDate(item.create_time);
          state.blogList.push(item);
        });
      }

      state.blogPage = blogData.page;
    },
    SET_KEYWORD(state, keyword) {
      state.keyword = keyword;
    },
  },
  actions: {
    async get_blog_data(
      { commit },
      {
        keyWord = "",
        categoryId = 0,
        page = 1,
        pageSize = 10,
        isInterserct = false,
      } = {}
    ) {
      let { data } = await $req.get({
        url: "blog",
        data: {
          keyWord,
          categoryId,
          page,
          pageSize,
        },
      });
      commit("GET_BLOG_DATA", {
        blogData: data.data,
        // 是否是无限下拉
        isInterserct,
      });
    },
  },
};
