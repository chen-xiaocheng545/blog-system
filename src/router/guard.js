import router from "./index";
import store from "@/store";
import nprogress from "nprogress";
import "nprogress/nprogress.css";

router.beforeEach((to, from, next) => {
  // 启动 nprogress
  nprogress.start();

  if (!store.getters.isLoggedIn) {
    // 未登录
    if (
      to.path === "/" ||
      to.path.startsWith("/detail") ||
      to.path.startsWith("/login")
    ) {
      next();
    } else {
      next({
        path: "login",
      });
    }
  } else {
    // 以登录
    next();
  }
});

router.afterEach(() => {
  // 关闭 nprogress
  nprogress.done();
});
