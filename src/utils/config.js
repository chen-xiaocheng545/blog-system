let baseUrl = "";
switch (process.env.NODE_ENV) {
  case "production":
    baseUrl = "http://116.205.183.75:3000";
    break;
  case "development":
    baseUrl = "http://localhost:3000";
    break;
  default:
    break;
}

export { baseUrl };
