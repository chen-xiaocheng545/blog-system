const sqlite3 = require("sqlite3").verbose();
const path = require("path");
const GenId = require("../utils/SnowFlake");

var db = new sqlite3.Database(path.join(__dirname, "./blog.sqlite3"));

// WorkerId 服务器的机器码
const genid = new GenId({ WorkerId: 1 });

// 封装promise
db.async = {};
// 查询方法
db.async.all = (sql, params) => {
  return new Promise((resolve, reject) => {
    db.all(sql, params, (err, rows) => {
      resolve({ err, rows });
    });
  });
};
// 修改/添加/删除 方法
db.async.run = (sql, params) => {
  return new Promise((resolve, reject) => {
    db.run(sql, params, (err, rows) => {
      resolve({ err, rows });
    });
  });
};

module.exports = {
  db,
  genid,
};
