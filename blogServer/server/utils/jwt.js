const { db, genid } = require("../db/DbUtils");

const jwtCheak = async (req, res, next) => {
  let { token } = req.headers;
  let admin_sql = "SELECT * FROM `admin` WHERE `token` = ?";
  let adminRes = await db.async.all(admin_sql, [token]);

  if (adminRes.err || adminRes.rows.length === 0) {
    res.send({
      code: 403,
      msg: "未登录",
    });
    return;
  } else {
    next();
  }
};

module.exports = {
  jwtCheak,
};
