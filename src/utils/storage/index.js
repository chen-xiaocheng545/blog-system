let local = window.localStorage;
let session = window.sessionStorage;
let set_local_storage = function (name, value) {
  local.setItem(name, value);
};
let set_session_storage = function (name, value) {
  session.setItem(name, value);
};
let get_local_storage = function (name) {
  return local.getItem(name);
};
let get_session_storage = function (name) {
  return session.getItem(name);
};
let remove_local_storage = function (name) {
  local.removeItem(name);
};
let remove_session_storage = function (name) {
  session.removeItem(name);
};

let getToken = function () {
  return local.getItem("token") || session.getItem("token");
};

export {
  set_local_storage,
  get_local_storage,
  remove_local_storage,
  getToken,
  set_session_storage,
  get_session_storage,
  remove_session_storage,
};
