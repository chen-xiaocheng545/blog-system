import Vue from "vue";
import {
  Button,
  Input,
  Container,
  Card,
  Popconfirm,
  Pagination,
  Menu,
  MenuItem,
  Aside,
  Main,
  Option,
  Form,
  FormItem,
  Select,
  Row,
  Table,
  TableColumn,
  Col,
  Dialog,
  Checkbox,
  Notification,
} from "element-ui";
import "element-ui/lib/theme-chalk/index.css";

const coms = [
  Button,
  Input,
  Container,
  Card,
  Popconfirm,
  Pagination,
  Menu,
  MenuItem,
  Aside,
  Main,
  Option,
  Form,
  FormItem,
  Select,
  Row,
  Table,
  TableColumn,
  Col,
  Dialog,
  Checkbox,
];

coms.forEach((com) => Vue.use(com));
Vue.prototype.$notify = Notification;
