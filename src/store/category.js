import Vue from "vue";
const { $req } = Vue.prototype;
export default {
  namespaced: true,
  state: {
    categoryList: [],
  },
  mutations: {
    SET_CATEGORY_LIST(state, data) {
      state.categoryList = data;
    },
  },
  actions: {
    async get_category_list({ commit }, palyload) {
      let { data } = await $req.get({
        url: "/category",
      });
      commit("SET_CATEGORY_LIST", data.data);
    },
  },
};
