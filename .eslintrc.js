module.exports = {
  root: true,
  env: {
    node: true,
  },
  // extends: ["@vue/standard", "plugin:vue/recommended"],
  parserOptions: {
    parser: "babel-eslint",
  },
  rules: {
    // "import/no-extraneous-dependencies": ["error", { devDependencies: true }],
    //关闭组件命名规则
    "vue/multi-word-component-names": "off",
  },
};
